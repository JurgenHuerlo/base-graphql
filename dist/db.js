"use strict";

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var dbOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
};

_mongoose["default"].connect('mongodb://localhost/base', dbOptions);

var connection = _mongoose["default"].connection;
connection.once('open', function () {
  console.log('DB is connected');
});
connection.once('error', function (err) {
  console.log(err);
  process.exit(0);
});