"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var UserSchema = new _mongoose.Schema({
  name: String,
  email: String,
  password: String
});
var User = (0, _mongoose.model)('user', UserSchema);
var _default = User;
exports["default"] = _default;