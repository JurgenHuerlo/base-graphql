"use strict";

require("@babel/polyfill");

var _app = _interopRequireDefault(require("./app"));

require("./db");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function main() {
  _app["default"].listen(_app["default"].get('port'));

  console.log('Server on port ', _app["default"].get('port'));
  console.log("Server is running on: http://localhost:".concat(_app["default"].get('port')));
}

main();