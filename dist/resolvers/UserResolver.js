"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.resolvers = void 0;

var _User = _interopRequireDefault(require("../models/User"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var resolvers = {
  Query: {
    message: function message(root) {
      console.log('Works');
    },
    getUsers: function () {
      var _getUsers = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(root) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _User["default"].find({});

              case 3:
                return _context.abrupt("return", _context.sent);

              case 6:
                _context.prev = 6;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 6]]);
      }));

      function getUsers(_x) {
        return _getUsers.apply(this, arguments);
      }

      return getUsers;
    }(),
    getUser: function () {
      var _getUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(root, _ref) {
        var id;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                id = _ref.id;
                _context2.prev = 1;
                _context2.next = 4;
                return _User["default"].findById({
                  _id: id
                });

              case 4:
                return _context2.abrupt("return", _context2.sent);

              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](1);
                console.log(_context2.t0);

              case 10:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[1, 7]]);
      }));

      function getUser(_x2, _x3) {
        return _getUser.apply(this, arguments);
      }

      return getUser;
    }(),
    getCountUsers: function () {
      var _getCountUsers = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(root) {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _User["default"].countDocuments({});

              case 3:
                return _context3.abrupt("return", _context3.sent);

              case 6:
                _context3.prev = 6;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 6]]);
      }));

      function getCountUsers(_x4) {
        return _getCountUsers.apply(this, arguments);
      }

      return getCountUsers;
    }()
  },
  Mutation: {
    createUser: function () {
      var _createUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(root, _ref2) {
        var input, nuevoUsuario;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                input = _ref2.input;
                _context4.prev = 1;
                nuevoUsuario = new _User["default"](input);
                _context4.next = 5;
                return nuevoUsuario.save();

              case 5:
                return _context4.abrupt("return", nuevoUsuario);

              case 8:
                _context4.prev = 8;
                _context4.t0 = _context4["catch"](1);
                console.log(_context4.t0);

              case 11:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[1, 8]]);
      }));

      function createUser(_x5, _x6) {
        return _createUser.apply(this, arguments);
      }

      return createUser;
    }(),
    updateUser: function () {
      var _updateUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(root, _ref3) {
        var input;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                input = _ref3.input;
                _context5.prev = 1;
                _context5.next = 4;
                return _User["default"].updateOne({
                  _id: input.id
                }, input);

              case 4:
                return _context5.abrupt("return", "The user with Id ".concat(input.id, " was updated"));

              case 7:
                _context5.prev = 7;
                _context5.t0 = _context5["catch"](1);
                console.log(_context5.t0);

              case 10:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[1, 7]]);
      }));

      function updateUser(_x7, _x8) {
        return _updateUser.apply(this, arguments);
      }

      return updateUser;
    }(),
    deleteUser: function () {
      var _deleteUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(root, _ref4) {
        var id;
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                id = _ref4.id;
                _context6.prev = 1;
                _context6.next = 4;
                return _User["default"].findOneAndDelete({
                  _id: id
                });

              case 4:
                return _context6.abrupt("return", "The user with Id ".concat(id, " was deleted"));

              case 7:
                _context6.prev = 7;
                _context6.t0 = _context6["catch"](1);
                console.log(_context6.t0);

              case 10:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[1, 7]]);
      }));

      function deleteUser(_x9, _x10) {
        return _deleteUser.apply(this, arguments);
      }

      return deleteUser;
    }()
  }
};
exports.resolvers = resolvers;