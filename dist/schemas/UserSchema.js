"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserSchema = UserSchema;

var _UserResolver = require("../resolvers/UserResolver");

var _graphqlImport = require("graphql-import");

var _graphqlTools = require("graphql-tools");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//En las nuevas versiones de las librerias usadas importSchema retorna una promesa asi que es necesario
//usar async y await
function UserSchema() {
  return _UserSchema.apply(this, arguments);
}

function _UserSchema() {
  _UserSchema = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var typeDefs, schema;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _graphqlImport.importSchema)('src/schemas/User.graphql');

          case 2:
            typeDefs = _context.sent;
            schema = (0, _graphqlTools.makeExecutableSchema)({
              typeDefs: typeDefs,
              resolvers: _UserResolver.resolvers
            });
            return _context.abrupt("return", schema);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _UserSchema.apply(this, arguments);
}