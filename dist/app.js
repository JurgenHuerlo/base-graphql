"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _expressGraphql = _interopRequireDefault(require("express-graphql"));

var _UserSchema = require("./schemas/UserSchema");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _express["default"])(); //Settings 

app.set('port', process.env.PORT || 5000); //routes

app.get('/', function (req, res) {
  return res.send('Working, edit code in src/app.js');
}); //Middlewares

app.use(_express["default"].json());
(0, _UserSchema.UserSchema)().then(function (schema) {
  app.use('/user', (0, _expressGraphql["default"])({
    schema: schema,
    graphiql: true
  }));
});
var _default = app;
exports["default"] = _default;