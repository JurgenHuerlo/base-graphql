import '@babel/polyfill';
import app from './app';
import './db';

function main(){
   app.listen(app.get('port'));
   console.log('Server on port ', app.get('port'));
   console.log(`Server is running on: http://localhost:${app.get('port')}`);
}

main();