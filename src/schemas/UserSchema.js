import { resolvers } from "../resolvers/UserResolver";
import { importSchema } from "graphql-import";
import { makeExecutableSchema } from "graphql-tools";

//En las nuevas versiones de las librerias usadas importSchema retorna una promesa asi que es necesario
//usar async y await
async function UserSchema(){
    const typeDefs =  await importSchema('src/schemas/User.graphql');
    const schema = makeExecutableSchema({typeDefs, resolvers});
    return schema;
}
export {UserSchema};