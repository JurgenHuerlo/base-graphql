import User from "../models/User";

export const resolvers = { 
    Query: {
        message: (root)=>{
            console.log('Works');
        },
        getUsers: async (root)=>{
            try {
                return await User.find({})
            } catch (error) {
                console.log(error);
            }
            
        },
        getUser: async (root, {id})=>{
            try {
                return await User.findById({_id:id});
            } catch (error) {
                console.log(error);
            }
        },

        getCountUsers: async (root)=>{
            try {
                return await User.countDocuments({});
            } catch (error) {
                console.log(error);
            }
            
        }
    },

    Mutation: {
        createUser: async (root, {input})=>{
            try {
                const nuevoUsuario = new User(input);
                await nuevoUsuario.save();
                return nuevoUsuario;
            }catch (e) {
                console.log(e);
            }
        },
        updateUser: async (root, {input})=>{
            try{
                await User.updateOne({_id: input.id}, input);
                return `The user with Id ${input.id} was updated`;
            }
            catch(e){
                console.log(e);
            }
        },
        deleteUser: async (root, {id})=>{
            try{
                await User.findOneAndDelete({ _id: id });
                return `The user with Id ${id} was deleted`;
            }
            catch(e){
                console.log(e);
            }

        }
    }
}