import express from 'express';
import graphqlHTTP from 'express-graphql';
import {UserSchema} from './schemas/UserSchema';

const app =express();

//Settings 
app.set('port', process.env.PORT || 5000);

//routes
app.get('/', (req, res)=> res.send('Working, edit code in src/app.js'));

//Middlewares
app.use(express.json());

UserSchema().then((schema)=>{
    app.use('/user', graphqlHTTP({
        schema: schema,
        graphiql: true
    }));
});


export default app;