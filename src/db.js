import mongoose from 'mongoose';

const dbOptions ={
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useCreateIndex:true,
}

mongoose.connect('mongodb://localhost/base', dbOptions);

const connection = mongoose.connection;

connection.once('open',()=>{
    console.log('DB is connected');
});

connection.once('error',err=>{
    console.log(err);
    process.exit(0);
});